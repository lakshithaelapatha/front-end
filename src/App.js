import React, { Component } from 'react';
import MessageList from './MsgView/MessageList';
import SendMsgForm from './MsgBox/SendMsgForm';
import './App.css';

class App extends Component {

  render() {
    return (
      <div className="App">
        <div className="container" style={{ marginTop: "20px" }}>
          <MessageList></MessageList>
          <SendMsgForm></SendMsgForm>
        </div>
      </div>
    );
  }
}

export default App;
