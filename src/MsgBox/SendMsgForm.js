import React, { Component } from 'react';
import axios from 'axios';

class SendMsgForm extends Component {

    constructor(props) {
        super(props);
        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const scope = this;
        axios.post('http://localhost:8080/ChatApp/rest/msg/send',
            {
                message: this.state.value,
                user: localStorage.getItem('rms')
            },
            {
                headers: { 'Content-Type': 'application/json' }
            })
            .then(function (response) {
                scope.setState({
                    value: ''
                });
            })
            .catch(function (error) {
                console.log(error);
                // alert('Unable to send this Message.');
            });
    }

    render() {
        return (
            <div className="row" >
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <form onSubmit={this.handleSubmit}>
                        <div className="input-group">
                            <input type="text" className="form-control has-info" value={this.state.value} onChange={this.handleChange} autoFocus placeholder="Message..."></input>
                            <span className="input-group-btn">
                                <button className="btn btn-primary" type="submit">Send
                                    <span className="glyphicon glyphicon-send"></span>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default SendMsgForm;