import React, { Component } from 'react';
import axios from 'axios';
import randomstring from 'randomstring'
import moment from 'moment';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import './MessageList.css';

class MessageList extends Component {

    constructor(props) {
        super(props);
        if (localStorage.getItem('rms') == null)
            localStorage.setItem('rms', randomstring.generate());
        this.state = { messages: [] };
        this.getMessages();
    }

    getMessages() {
        let self = this;
        axios.get('http://localhost:8080/ChatApp/rest/msg',
            {
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                self.setState({
                    messages: response.data
                });
            }).catch(function (error) {
                console.log(error);
            });
    }

    deleteMessage(id) {
        confirmAlert({
            title: 'Are you sure?',
            message: 'You want to delete this Message?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        axios.delete('http://localhost:8080/ChatApp//rest/msg/delete/' + id,
                            {
                                headers: { 'Content-Type': 'application/json' }
                            }).catch(function (error) {
                                console.log(error);
                            });
                    }
                },
                {
                    label: 'No'

                }
            ]
        });
    }

    componentDidMount() {
        var self = this;
        setInterval(function () {
            self.getMessages();
        }, 1000);

    }

    render() {
        let Message = function (props) {
            /*eslint no-cond-assign: "error"*/

            switch (props.value.user) {
                case localStorage.getItem('rms'):
                    return (
                        <div className="row">
                            <div className="col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <div className="alert alert-success text-left">
                                    <button type="button" className="close" onClick={() => props.handler(props.value.id)} data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {props.value.message} <p><small>{moment(props.value.sentDate).format("llll")}</small></p>
                                </div>
                            </div>
                        </div>
                    );
                default:
                    return (
                        <div className="row">
                            <div className="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                                <div className="alert alert-info text-left">
                                    <button type="button" className="close" onClick={() => props.handler(props.value.id)} data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {props.value.message} <p><small>{moment(props.value.sentDate).format("llll")}</small></p>
                                </div>
                            </div>
                        </div>
                    );
            }

        }

        let MessageList = function (props) {
            return (props.messages.map((element) => <Message key={element.id} value={element} handler={props.handler} />));
        }

        return (
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div className="panel panel-default">
                        <div className="panel-body">
                            <MessageList messages={this.state.messages} handler={this.deleteMessage}></MessageList>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default MessageList;